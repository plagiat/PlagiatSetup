#encoding: utf-8
import sys
sys.path.append("../Controls/Mididings/")

from ports import *

connasses_metallica_clignote = [
    [['/BC/Segment/All', 200, 200, 200],['/BJ/Segment/All', 200, 200, 200]],None,[['/BC/Segment/All', 0, 0, 0],['/BJ/Segment/All', 0, 0, 0]],None,
    None,None,None,None,
    None,None,None,None,
    None,None,None,None
]
